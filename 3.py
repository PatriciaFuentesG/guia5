archivo = "co2_emission.csv"
(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)
def abrir():
    temp = open(archivo)
    diccionario = {}

    for contador,linea in enumerate(temp):
        if contador != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
    temp.close()
    return diccionario
def mostrar_pais(data):
    suma = 0
    total = total_paises(data)
    for key, value in data.items():
        pais = data[key]
        for key, value in pais.items():
            if key != "codigo":
                suma = suma + float(value)

    promedio = suma /total
    print(f"El total emitido por todos los paises de toneladas de CO2 es {suma} y su promedio es de {promedio}")


def total_paises(data):
    total = len(data)
    return total

def main():
    dic =  abrir()
    mostrar_pais(data=dic)


main()

