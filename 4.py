archivo = "co2_emission.csv"            

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)
def abrir():
    temp = open(archivo)
    diccionario = {}
    
    for contador,linea in enumerate(temp):
        if contador != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
    temp.close()
    return diccionario
def cantidad_de_muestras(data):
    mayor = 0
    for key, value in data.items():
        pais = data[key]
        pais_mayor = key
        contador = 0
        sumador = 0
        for key, value in pais.items():
            if key != "codigo":
                contador = contador + 1
            if key != "codigo":
                sumador = sumador + float(value)
        promedio = sumador/contador        
        if promedio > mayor:
            mayor = promedio
            cantidad_mayor = pais_mayor        
    print(f"El pais que en promedio a emitido mas toneladas es  {cantidad_mayor} con {mayor}ton")             

def main():
    dic =  abrir()
    cantidad_de_muestras(data=dic)

main()

