archivo = "co2_emission.csv"
(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)
def abrir():
    temp = open(archivo)
    diccionario = {}

    for contador,linea in enumerate(temp):
        if contador != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
    temp.close()
    return diccionario
def cantidad_de_muestras(data):
    menor = 268
    contador = 0
    for key, value in data.items():
        pais = data[key]
        pais_menor = key   
        contador = 0
        for key, value in pais.items():
            if key != "codigo":
                contador = contador + 1
        if contador < menor:                                                            
            menor = contador                                                                                
            cantidad_menor = pais_menor
            

    print(f"El pais con mayor cantidad de regitros de estudios es {cantidad_menor} con {menor} muestras")              

def main():
    dic =  abrir()
    cantidad_de_muestras(data = dic)
main()

