archivo = "co2_emission.csv"   
(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)
def abrir():
    temp = open(archivo)
    diccionario = {}

    for contador,linea in enumerate(temp):
        if contador != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
    temp.close()
    return diccionario
def buscar_pais():
    pais = input("Ingrese un país: ")
    return pais.capitalize()


def cantidad_de_muestras(data, pais):
    diccionario = data[pais]
    suma = 0
    for key, value in diccionario.items():
        # print(key, value)
        if key != "codigo":
            suma = suma + float(value)

    print(f"El total emitido por {pais} de toneladas de CO2 es {suma}")

def menu (data):
    print("-----Menu----\n1.Para ver emiciones de Co2 de un pais\n2.Salir")
    elegir= str(input(""))
    if elegir == "1":
        pais = buscar_pais()
        cantidad_de_muestras(data=data, pais=pais)
    else:
        print("Se ha finalizado el programa")

def main():
    dic =  abrir()
    menu(dic)
main()    
