archivo = "co2_emission.csv"
(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)
def abrir():
    temp = open(archivo)
    diccionario = {}

    for contador,linea in enumerate(temp):
        if contador != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
    temp.close()
    return diccionario
def promedio_total(data):
    total = total_paises(data)
    suma = 0
    for key, value in data.items():
        pais = data[key]
        for key, value in pais.items():
            if key != "codigo":
                suma = suma + float(value)
    promedio = suma /total
    return promedio

def promedio_parcial(data):
    margen = {}
    total = total_paises(data) 
    promedio_t=promedio_total(data)
    nueve = (promedio_t*9)/100
    once = (promedio_t*11)/100
    for key, value in data.items():
        pais = data[key]
        pais_ = key
        sumador = 0
        for key, value in pais.items():
            if key != "codigo":
                sumador = sumador + float(value)
        promedio = sumador/total
        if (promedio > nueve) and (promedio < once):
             margen[pais_]=promedio
    print(f"En el margen desde {nueve} hasta {once} esta : ")
    for key , value in margen.items() :
        print ( f"El pais de {key} con un promedio de {value} de CO2 " )
     

def total_paises(data):
    total = len(data)
    return total

def main():
    dic =  abrir()
    promedio_total(data=dic)
    promedio_parcial(data=dic)


main()


